package org.springframework.samples.petclinic;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CasoUso1Test {
	
	@Test
	public void probarSumar() {
		CasoUso1 caso = new CasoUso1();
		
		assertEquals(4 ,caso.sumar(2, 2));
	}

}
