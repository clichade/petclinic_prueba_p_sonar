package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;


public class VetRepositoryTest {
	
	
	
	private Vet vet;
	
	@Before
	public void init() {
		//Collection <Vet> cVets = 
		
		if (this.vet==null) {
			vet = new Vet ();
			vet.setFirstName("Javier");
			vet.setLastName("Berrocal");
			
			
		}
	}
	
	
	
	@Test
	public void testFindById() {
		assertEquals(1,1);
		
	}
	
	@Test
	public void testFindByIdNotEqual() {
		assertEquals(1,1);	
	}
	
	
	
	@After
	public void finish() {
		this.vet=null;
	}
	

}
