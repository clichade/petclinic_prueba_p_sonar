package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.function.ToDoubleFunction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;



public class PetTest {
	
	public Pet pet;
	

	@Before
	public void init() {
		if(this.pet==null) {
			
			pet = new Pet();
			pet.setName("Mascota de prueba");
			pet.setWeight(1);
			pet.setComments("1");
		}	
	}
	
	@Test
	public void testAttr() {
	
		
		assertEquals(pet.getWeight(),1,.8);
		assertEquals(pet.getComments(),"1");
		
		pet.setWeight(2);
		pet.setComments("2");
		
		assertEquals(pet.getWeight(), 2,.8);
		assertEquals(pet.getComments(),"2");
		
	}

}
